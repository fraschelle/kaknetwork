#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construction of a Kak Network of instantaneoulsy trained neurons.
"""

import numpy as np

def input_layer(x):
    addx = np.ones(x.shape[0], dtype=int).reshape(x.shape[0], 1)
    return np.hstack([x, addx]) 

def hidden_layer(x):
    """Instantaneously learned hidden layer. Hidden layer is the first layer of the KakNetwork, it has dimension NxI, with N the number of examples and I the input dimension."""
    return np.hstack([2*x-1, -(np.sum(x, axis=1)-1).reshape(x.shape[0], 1)]) 

def output_layer(y):
    """Instantaneously learned output layer. Ouput layer has dimension OxN, with O the output dimension and N the number of examples."""
    out = 2*y-1 
    return out.T 

def sigma(x, axis=1):
    """Non-linearity of the neuron"""
    mask = np.sum(x, axis=axis) > 0
    return np.array(mask, dtype=int)

def train(x, y):
    """Train the KakNetwork. x is a NxI array, and y is a NxO array, with I and O the input/output dimensions, respectively, and N is the number of examples. Return the arrays of the hidden and output layers after the learning process."""
    if len(y.shape) == 1:
        y = y.reshape((len(y), 1))
    return hidden_layer(x), output_layer(y)

def predict(x, hidden, output): 
    """Predict the value of the x input through the KakNetwork constructed with the hidden and output layers from the train method."""
    x1 = np.concatenate([x, [1]]) # concatenate the bias as 1 
    x1 = x1.reshape(len(x1), 1) 
    x2 = sigma(np.dot(hidden, x1), axis=1) 
    x2 = x2.reshape(len(x2), 1) 
    x3 = sigma(np.dot(output, x2), axis=1)
    return x3

#%%

# input example
x = np.array([[0,0,1],[0,1,0],[0,1,1],[1,0,0],[1,0,1],[1,1,0],[1,1,1]])
# output example
y = np.array([1,1,0,1,0,0,1])

wh, wo = train(x, y)

for i in range(x.shape[0]):
    x_ = predict(x[i,:], wh, wo) 
    assert x_ == y[i]

# all together is not possible simply
X = input_layer(x)

# construct a fake example 

x = np.random.randint(0,2, size=(12,4))
y = np.random.randint(0,2, size=(12,5))

wh, wo = train(x, y) 

for i in range(x.shape[0]):
    y_ = predict(x[i,:], wh, wo) 
    if not all([y_[j] == y[i][j] for j in range(len(x_))]):
        for i_ in range(x.shape[0]):
            for j_ in range(x.shape[0]):
                if i_==j_:
                    continue
                if all([x[i_,k] == x[j_,k] for k in range(x.shape[1])]):
                    print(x[i_,:], x[j_,:])
                    print(y[i_], y[j_])
                    print(y_)
                    break 
                    



